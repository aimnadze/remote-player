<?php

function create_control_buttons ($address, $key) {
    $queryString = htmlspecialchars(http_build_query([
        'address' => $address,
        'key' => $key,
    ]));
    return 
        '<div style="margin-top: 24px">'
            ."<a class=\"button\" href=\"submit-play.php?$queryString\">Play</a>"
            ."<a class=\"button\" href=\"submit-pause.php?$queryString\">Pause</a>"
        .'</div>';
}
