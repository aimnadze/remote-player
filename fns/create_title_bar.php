<?php

function create_title_bar ($parsedAddress) {
    return
        '<div class="page-title">'
            .'<div style="position: relative">'
                .'<h1 class="page-heading">'
                    .'<span class="page-logo"></span>'
                    .htmlspecialchars($parsedAddress)
                .'</h1>'
                .'<div style="position: absolute; top: 0; right: 0">'
                    .'<a class="button iconButton collapsible quitButton" href="connect.php" title="Quit">'
                        .'<span class="iconButton-icon exit"></span>'
                        .'<span class="iconButton-text">Quit</span>'
                    .'</a>'
                .'</div>'
            .'</div>'
        .'</div>';
}
