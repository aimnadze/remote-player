<?php

include_once 'fns/redirect.php';
include_once 'fns/request_strings.php';
include_once 'lib/api.php';

list($filename, $title) = request_strings('filename', 'title');

$response = $api->enqueue($filename, $title);

if (!$response) {
    redirect('player.php?'.http_build_query([
        'address' => $address,
        'key' => $key,
        'error' => 'enqueue-failed',
    ]));
}

redirect('player.php?'.http_build_query([
    'address' => $address,
    'key' => $key,
]));
