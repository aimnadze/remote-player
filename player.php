<?php

function create_play_pause_buttons ($paused, $queryString) {
    if ($paused) {
        return
            '<a class="button iconButton collapsible" title="Play"'
            ." href=\"submit-play.php?$queryString\">"
                .'<span class="iconButton-icon play"></span>'
                .'<span class="iconButton-text">Play</span>'
            .'</a>'
            .'<span title="Pause" style="margin-left: 4px"'
            .' class="button iconButton collapsible disabled">'
                .'<span class="iconButton-icon pause"></span>'
                .'<span class="iconButton-text">Pause</span>'
            .'</span>';
    }
    return
        '<span class="button iconButton collapsible disabled" title="Play">'
            .'<span class="iconButton-icon play"></span>'
            .'<span class="iconButton-text">Play</span>'
        .'</span>'
        .'<a class="button iconButton collapsible" title="Pause"'
        ." href=\"submit-pause.php?$queryString\""
        .' style="margin-left: 4px">'
            .'<span class="iconButton-icon pause"></span>'
            .'<span class="iconButton-text">Pause</span>'
        .'</a>';
}

function create_playlist ($audioFiles, $address, $key) {
    if (!$audioFiles) return 'No files.';
    $html = '<ul>';
    foreach ($audioFiles as $audioFile) {
        $removeHref = htmlspecialchars('submit-remove-audio-file.php?'.http_build_query([
            'address' => $address,
            'key' => $key,
            'id' => $audioFile->id,
        ]));
        $html .=
            '<li class="listItem" style="position: relative">'
                .'<div class="playlistItem-content">'
                    .'<div class="songTitle">'
                        .htmlspecialchars($audioFile->title)
                    .'</div>'
                    .'<div class="displayLink">'
                        .htmlspecialchars($audioFile->filename)
                    .'</div>'
                .'</div>'
                .'<div class="playlistItem-buttons">'
                    .'<a class="button iconButton collapsible" title="Remove"'
                    ." href=\"$removeHref\" style=\"width: 100%\">"
                        .'<span class="iconButton-icon remove"></span>'
                        .'<span class="iconButton-text">Remove</span>'
                    .'</a>'
                .'</div>'
            .'</li>';
    }
    $html .= '</ul>';
    return $html;
}

include_once 'fns/create_control_buttons.php';
include_once 'fns/create_title_bar.php';
include_once 'lib/api.php';
include_once 'lib/revisions.php';

$response = $api->state();

if (!$response) {
    include_once 'fns/redirect.php';
    redirect('./connect.php?'.http_build_query([
        'address' => $address,
        'key' => $key,
        'error' => 'failed',
    ]));
}

$queryString = htmlspecialchars(http_build_query([
    'address' => $address,
    'key' => $key,
]));

header('Cache-Control: no-cache, no-store');
header('Content-Type: text/html; charset=UTF-8');

echo '<!DOCTYPE html>'
    .'<html>'
        .'<head>'
            .'<title>'.htmlspecialchars($parsedAddress).'</title>'
            .'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'
            .'<meta content="width=device-width" name="viewport">'
            .'<link rel="icon" type="image/png" href="images/favicon.png?'.$revisions['images/favicon.png'].'" />'
            .'<link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32.png" />'
            .'<link rel="stylesheet" type="text/css" href="common.css?'.$revisions['common.css'].'" />'
            .'<link rel="stylesheet" type="text/css" href="player.css?5" />'
            .'<link rel="stylesheet" type="text/css" href="page.css?'.$revisions['page.css'].'" />'
        .'</head>'
        .'<body>'
            .create_title_bar($parsedAddress)
            .'<div class="page-body">'
                .'<div>'
                    .create_play_pause_buttons($response->paused, $queryString)
                    .'<span style="margin-left: 8px; line-height: 32px">'
                        ."Volume: $response->volume%"
                    .'</span>'
                    .'<a class="button iconButton collapsible"'
                    ." href=\"submit-volume-up.php?$queryString\""
                    .' style="margin-left: 4px" title="Volume Up">'
                        .'<span class="iconButton-icon volume-up"></span>'
                        .'<span class="iconButton-text">Up</span>'
                    .'</a>'
                    .'<a class="button iconButton collapsible"'
                    ." href=\"submit-volume-down.php?$queryString\""
                    .' style="margin-left: 4px" title="Volume Down">'
                        .'<span class="iconButton-icon volume-down"></span>'
                        .'<span class="iconButton-text">Down</span>'
                    .'</a>'
                .'</div>'
                .'<div class="page-newpanel">'
                    .'<h2 style="margin-bottom: 8px">Playlist</h2>'
                    .create_playlist($response->audioFiles, $address, $key)
                .'</div>'
                .'<div style="margin: 12px -12px 12px -12px; white-space: normal">'
                    .'<form action="search-oggge.php" class="panel">'
                        .'<input type="hidden" name="address" value="'.htmlspecialchars($address).'" />'
                        .'<input type="hidden" name="key" value="'.htmlspecialchars($key).'" />'
                        .'<div>'
                            .'<h2>Enqueue from OGG.GE</h2>'
                        .'</div>'
                        .'<div style="margin-top: 8px; position: relative; height: 32px">'
                            .'<div class="ogggeSearchKeywordWrapper">'
                                .'<input type="text" name="keyword"'
                                .' required="required" class="textinput"'
                                .' placeholder="Search songs here..."'
                                .' style="width: 100%; height: 100%" />'
                            .'</div>'
                            .'<div class="ogggeSearchButtonWrapper">'
                                .'<button class="button iconButton collapsible" title="Search" style="width: 100%">'
                                    .'<span class="iconButton-icon search"></span>'
                                    .'<span class="iconButton-text">Search</span>'
                                .'</button>'
                            .'</div>'
                        .'</div>'
                    .'</form>'
                    .'<form action="submit-enqueue.php" method="post" class="panel">'
                        .'<input type="hidden" name="address" value="'.htmlspecialchars($address).'" />'
                        .'<input type="hidden" name="key" value="'.htmlspecialchars($key).'" />'
                        .'<div>'
                            .'<h2>Enqueue from URL</h2>'
                        .'</div>'
                        .'<div style="margin-top: 8px; position: relative; height: 32px">'
                            .'<div class="enqueueUrlWrapper">'
                                .'<input type="text" name="filename" id="filenameInput"'
                                .' class="textinput" placeholder="Enter a URL here..."'
                                .' style="width: 100%; height: 100%" />'
                            .'</div>'
                            .'<div class="enqueueButtonWrapper">'
                                .'<button class="button iconButton collapsible" title="Enqueue" style="width: 100%">'
                                    .'<span class="iconButton-icon enqueue"></span>'
                                    .'<span class="iconButton-text">Enqueue</span>'
                                .'</button>'
                            .'</div>'
                        .'</div>'
                    .'</form>'
                .'</div>'
            .'</div>'
            .'<script type="text/javascript">'
                ."var stateSequence = $response->sequence;"
                .'var address = '.json_encode($address).';'
                .'var key = '.json_encode($key).';'
            .'</script>'
            .'<script type="text/javascript" src="player.js?1"></script>'
        .'</body>'
    .'</html>';
