<?php

include_once '../fns/request_strings.php';
include_once '../lib/api.php';

list($sequence) = request_strings('sequence');

$response = $api->waitState($sequence);

echo json_encode($response);
