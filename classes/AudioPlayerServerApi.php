<?php

class AudioPlayerServerApi {

    private $httpBase;
    private $key;
    public $lastError;
    public $lastResponse;

    function __construct ($httpBase, $key) {
        $this->httpBase = $httpBase;
        $this->key = $key;
    }

    function enqueue ($filename, $title = '') {
        return $this->post('enqueue', [
            'filename' => $filename,
            'title' => $title,
        ]);
    }

    function pause () {
        return $this->post('pause');
    }

    function play () {
        return $this->post('play');
    }

    private function post ($method, $params = [], $timeout = 20) {
        $params['key'] = $this->key;
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $this->httpBase.$method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => http_build_query($params),
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => $timeout,
        ]);
        $this->lastResponse = $response = curl_exec($ch);
        $this->lastError = curl_error($ch);
        return json_decode($response);
    }

    function removeAudioFile ($id) {
        return $this->post('removeAudioFile', ['id' => $id]);
    }

    function state () {
        return $this->post('state');
    }

    function waitState ($sequence) {
        return $this->post('wait-state', [
            'sequence' => $sequence,
        ], 60);
    }

    function volumeDown () {
        return $this->post('volumeDown');
    }

    function volumeUp () {
        return $this->post('volumeUp');
    }

}
