<?php

class OggGeApi {

    private $base = 'http://ogg.ge/api/';
    public $lastError;
    public $lastResponse;

    private function post ($method, $params) {
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => "{$this->base}$method.php",
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 20,
        ]);
        $this->lastResponse = $response = curl_exec($ch);
        $this->lastError = curl_error($ch);
        return json_decode($response);
    }

    function searchSongs ($keyword) {
        return $this->post('search-songs', ['keyword' => $keyword]);
    }

}
