<?php

include_once 'lib/defaults.php';
include_once 'lib/revisions.php';
include_once 'fns/request_strings.php';

list($address, $key, $error) = request_strings('address', 'key', 'error');

if ($error == 'failed') {
    $errorPanel =
        '<div class="errorPanel">'
            .'<div style="font-weight: bold">Error!</div>'
            .'Failed to connect to the server.'
        .'</div>';
} else {
    $errorPanel = '';
}

header('Cache-Control: no-cache, no-store');
header('Content-Type: text/html; charset=UTF-8');

echo '<!DOCTYPE html>'
    .'<html>'
        .'<head>'
            .'<title>Remote Player</title>'
            .'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'
            .'<meta content="width=device-width" name="viewport">'
            .'<link rel="icon" type="image/png" href="images/grey-favicon.png" />'
            .'<link rel="icon" type="image/png" sizes="32x32" href="images/grey-favicon-32.png" />'
            .'<link rel="stylesheet" type="text/css" href="common.css?'.$revisions['common.css'].'" />'
            .'<link rel="stylesheet" type="text/css" href="connect.css" />'
        .'</head>'
        .'<body style="text-align: center">'
            .'<div style="display: inline-block; vertical-align: middle; height: 100%">'
            .'</div>'
            .'<div style="display: inline-block; vertical-align: middle; text-align: left">'
                .'<form action="player.php">'
                    .'<h1 style="margin: 8px; margin-bottom: 16px">'
                        .'<span class="logo"></span>'
                        .'Remote Player'
                    .'</h1>'
                    .$errorPanel
                    .'<div style="margin: 8px">'
                        .'<div>'
                            .'<label for="addressInput">Address:</label>'
                        .'</div>'
                        .'<div style="margin-top: 4px">'
                            .'<input type="text" name="address"'
                            .' id="addressInput" class="textinput"'
                            .' placeholder="'.htmlspecialchars($defaults['address']).'"'
                            .' value="'.htmlspecialchars($address).'" />'
                        .'</div>'
                    .'</div>'
                    .'<div style="margin: 8px">'
                        .'<div>'
                            .'<label for="keyInput">Key:</label>'
                        .'</div>'
                        .'<div style="margin-top: 4px">'
                            .'<input type="text" name="key"'
                            .' id="keyInput" class="textinput"'
                            .' value="'.htmlspecialchars($key).'" />'
                        .'</div>'
                    .'</div>'
                    .'<div style="margin: 8px">'
                        .'<input type="submit" value="Connect" class="button" />'
                    .'</div>'
                .'</form>'
            .'</div>'
        .'</body>'
    .'</html>';
