<?php

include_once 'fns/redirect.php';
include_once 'lib/api.php';

$response = $api->volumeUp();

if (!$response) {
    redirect('player.php?'.http_build_query([
        'address' => $address,
        'key' => $key,
        'error' => 'volume-up-failed',
    ]));
}

redirect('player.php?'.http_build_query([
    'address' => $address,
    'key' => $key,
]));
