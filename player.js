(function (address, key, sequence) {

    function send () {
        var request = new XMLHttpRequest
        request.open('post', 'api/wait-state.php')
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
        request.responseType = 'json'
        request.send(
            'address=' + encodeURIComponent(address) +
            '&key=' + encodeURIComponent(key) +
            '&sequence=' + sequence
        )
        request.onload = function () {
            var response = request.response
            if (response) location.reload()
            else setTimeout(send, 1000)
        }
    }

    send()

})(address, key, stateSequence)
