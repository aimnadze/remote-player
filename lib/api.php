<?php

include_once __DIR__.'/../fns/request_strings.php';
include_once __DIR__.'/../classes/AudioPlayerServerApi.php';
list($address, $key) = request_strings('address', 'key');
$parsedAddress = preg_replace('/\s/', '', $address);
if ($parsedAddress === '') {
    include_once 'defaults.php';
    $parsedAddress = $defaults['address'];
}
$httpBase = "http://$parsedAddress";
if (substr($httpBase, -1) !== '/') $httpBase .= '/';
$api = new AudioPlayerServerApi($httpBase, $key);
