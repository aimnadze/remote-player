<?php

include_once 'fns/redirect.php';
include_once 'fns/request_strings.php';
include_once 'lib/api.php';

list($id) = request_strings('id');

$api->removeAudioFile($id);
redirect('player.php?'.http_build_query([
    'address' => $address,
    'key' => $key,
    'message' => 'audio-file-removed',
]));
