<?php

include_once 'fns/redirect.php';
include_once 'lib/api.php';

$response = $api->play();

if (!$response) {
    redirect('player.php?'.http_build_query([
        'address' => $address,
        'key' => $key,
        'error' => 'play-failed',
    ]));
}

redirect('player.php?'.http_build_query([
    'address' => $address,
    'key' => $key,
]));
