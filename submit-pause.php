<?php

include_once 'fns/redirect.php';
include_once 'lib/api.php';

$response = $api->pause();

if (!$response) {
    redirect('player.php?'.http_build_query([
        'address' => $address,
        'key' => $key,
        'error' => 'pause-failed',
    ]));
}

redirect('player.php?'.http_build_query([
    'address' => $address,
    'key' => $key,
]));
