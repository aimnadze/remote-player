<?php

function createSongTitle ($song) {

    $albumArtistNames = [];
    $featuredArtistNames = [];
    foreach ($song->songArtists as $songArtist) {
        $name = $songArtist->artist->name;
        if ($songArtist->featured) {
            $featuredArtistNames[] = $name;
        } else {
            $albumArtistNames[] = $name;
        }
    }
    $albumArtistNames = join(', ', $albumArtistNames);
    $featuredArtistNames = join(', ', $featuredArtistNames);

    return
        ($albumArtistNames ? "$albumArtistNames - " : '')
        .$song->name
        .($featuredArtistNames ? " (feat. $featuredArtistNames)" : '');

}

function createEnqueueUrl ($song, $address, $key) {
    return 'submit-enqueue.php?'.http_build_query([
        'address' => $address,
        'key' => $key,
        'filename' => createSongUrl($song),
        'title' => createSongTitle($song),
    ]);
}

function createSongUrl ($song) {
    return "http://ogg.ge/ogg-files/get.php?id={$song->oggFile->id}";
}

function createList ($songs, $address, $key) {
    if ($songs) {
        $html = '<ul class="ul">';
        foreach ($songs as $song) {
            if ($song->oggFile) {

                $enqueueUrl = createEnqueueUrl($song, $address, $key);
                $href = htmlspecialchars($enqueueUrl);

                $html .=
                    '<li class="listItem" style="position: relative">'
                        .'<div class="listItem-content">'
                            .'<div class="songTitle">'
                                .htmlspecialchars(createSongTitle($song))
                            .'</div>'
                            .'<div class="displayLink">'
                                .createSongUrl($song)
                            .'</div>'
                        .'</div>'
                        .'<div class="listItem-buttons">'
                            .'<a class="button iconButton collapsible" title="Enqueue"'
                            ." href=\"$href\" style=\"width: 100%\">"
                                .'<span class="iconButton-icon enqueue"></span>'
                                .'<span class="iconButton-text">Enqueue</span>'
                            .'</a>'
                        .'</div>'
                    .'</li>';

            }
        }
        $html .= '</ul>';
        return $html;
    }
    return 'No songs found.';
}

include_once 'fns/create_title_bar.php';
include_once 'fns/request_strings.php';
include_once 'lib/api.php';
include_once 'lib/revisions.php';
include_once 'classes/OggGeApi.php';

list($keyword) = request_strings('keyword');

$api = new OggGeApi;
$response = $api->searchSongs($keyword);
if (!$response) {
    // TODO handle network error
    var_dump($api->lastResponse);
    var_dump($api->lastError);
    die;
}

$songs = $response->songs;
$songs = array_filter($songs, function ($song) {
    return $song->oggFile;
});

$bang = in_array('!', explode(' ', $keyword));

if ($bang && count($songs) == 1) {
    include_once 'fns/redirect.php';
    redirect(createEnqueueUrl($songs[0], $address, $key));
}

$queryString = htmlspecialchars(http_build_query([
    'address' => $address,
    'key' => $key,
]));

header('Cache-Control: no-cache, no-store');
header('Content-Type: text/html; charset=UTF-8');

echo
    '<!DOCTYPE html>'
    .'<html>'
        .'<head>'
            .'<title>'.htmlspecialchars($parsedAddress).'</title>'
            .'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'
            .'<meta content="width=device-width" name="viewport">'
            .'<link rel="icon" type="image/png" href="images/favicon.png?'.$revisions['images/favicon.png'].'" />'
            .'<link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32.png" />'
            .'<link rel="stylesheet" type="text/css" href="common.css?'.$revisions['common.css'].'" />'
            .'<link rel="stylesheet" type="text/css" href="search-oggge.css?5" />'
            .'<link rel="stylesheet" type="text/css" href="page.css?'.$revisions['page.css'].'" />'
        .'</head>'
        .'<body>'
            .create_title_bar($parsedAddress)
            .'<div class="page-body">'
                .'<div>'
                    .'<a class="button iconButton" title="Back"'
                    ." href=\"player.php?$queryString\">"
                        .'<span class="iconButton-icon back"></span>'
                        .'<span class="iconButton-text">Back</span>'
                    .'</a>'
                .'</div>'
                .'<form class="searchPanel page-newpanel" action="search-oggge.php">'
                    .'<input type="hidden" name="address" value="'.htmlspecialchars($address).'" />'
                    .'<input type="hidden" name="key" value="'.htmlspecialchars($key).'" />'
                    .'<div>'
                        .'<h2>Enqueue from OGG.GE</h2>'
                    .'</div>'
                    .'<div style="margin-top: 8px; position: relative; height: 32px">'
                        .'<div class="searchKeywordWrapper">'
                            .'<input type="text" name="keyword" class="textinput"'
                            .' placeholder="Search songs here..."'
                            .' required="required" autofocus="autofocus"'
                            .' value="'.htmlspecialchars($keyword).'"'
                            .' style="width: 100%; height: 100%" />'
                        .'</div>'
                        .'<div class="searchButtonWrapper">'
                            .'<button class="button iconButton collapsible" style="width: 100%" title="Search">'
                                .'<span class="iconButton-icon search"></span>'
                                .'<span class="iconButton-text">Search</span>'
                            .'</button>'
                        .'</div>'
                    .'</div>'
                .'</form>'
                .'<div class="page-newpanel">'
                    .createList($response->songs, $address, $key)
                .'</div>'
            .'</div>'
        .'</body>'
    .'</html>';
